
import java.util.InputMismatchException;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author natthawadee
 */
    import java.util.InputMismatchException;
import java.util.Scanner;

public class TestOx {

    static private Scanner sc = new Scanner(System.in);
    static Ox table = new Ox();
    static ClassOx  o = new ClassOx ("O");
    static ClassOx x = new ClassOx ("X");

    public static void main(String[] args) {
        System.out.println("Welcome to OX Game");
        System.out.println(table);
        for (int i = 0; i < 9; i++) {
            try {
                if (i % 2 == 0) {
                    if (turn(o)) {
                        System.out.println(table);
                        System.out.println(">>>O Win<<<");
                        return;
                    }
                } else {
                    if (turn(x)) {
                        System.out.println(table);
                        System.out.println(">>>X Win<<<");
    
                        return;
                    }
                }
            } catch (IllegalStateException e) {
                --i;
                System.err.println(e + " Try again [Select unsed location]");
            } catch (IllegalArgumentException e) {
                --i;
                System.err.println(e + " Try again [Use number in {1, 2 ,3}]");
            } catch (InputMismatchException e) {
                --i;
                System.err.println(e + " Try again [Number only]");
                sc.next();
            }
            System.out.println(table);
        }

        System.out.println(">>>Draw<<<");

    }

    static Boolean turn(ClassOx classox) {
        System.out.printf("Turn %s\n",  classox.toString());
        System.out.println("Please input row, col:");
        int row = sc.nextInt();
        int column = sc.nextInt();
        return table.set( classox, row, column);
    }

}
